# SCRIP INSTRUCTIONS

## ./renamelink.sh

Bash script to use on unix systems script should be called from the **oxygen2 repo base dir**

It creates a symlink on the same folder that the file provided as argument.
The file is renamed and the symlink preserves the old name.

**Usage:**
- Give execution permission with `chmod +x renamelink.sh`
- Usage is shown when called without arguments `./renamelink.sh`


## movelinks.py

Python script that places all the symlinks in icon folders to the `symbolic` folder.

**Usage:**

- You need python installed to make it work, try `python3 --version` or `python --version` 
- Open a terminal and type `python3 movelinks.py`
- A `movelinks.log` is generated please don't commit it to the repo.


## Intended icon rename: 2023-06-11

    ./renamelink.sh
    python3 movelinks.py
    

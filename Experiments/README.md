# Design Language Experimentation

<br/>

### Basic design language of 16X16 action icons:

- Use simple geometry (where possible) to communicate the intended metaphor.
- Always pixel aligned.
- 16x16 icons ought to strictly use 1pt width strokes.
- Use a single stroke to draw the metaphor.
- Utilize looping the stroke (preferably just one time) to add detail to the design. This is reminiscent of illustrations of wind or moving air, hence the name, Oxygen.
- The stroke should not connect end to end or intersect itself unless absolutely necessary. Instead there should be 1 to 1.5px of negative space surround stroke ends and intersections.
- Rounded corners are drawn to taste but should be larger than 1px radius to avoid sharp corners when scaled at 100%. If the corner is drawn for detail rather than the fundamental silhouette then the radius can be smaller but no smaller than .5 px.
- The end-caps basic shape is an un-symmetrical rounded cap where one corner of the stroke’s end has a radius of .75 px and the sibling corner is rounded at .25 px. The orientation of the end cap is dependent on the metaphors design and is left to the discretion of the designer.
- ~~Duo-toned~~
# rtl proposals

Proposals for designin a different icon on rtl representations.

~~~

FORMAT OF PROPOSALS: ltr Icon /rtl version

format-justify-left       / format-justify-right
edit-clar-locationbar-lrt / edit-clar-locationbar-rtl
go-last-view              / go-next-view
go-last                   / go-next
go-next                   / go-last

~~~
RTL NEEDED

|Name  |reason| |
|------------------------------|-----------------|---|
|... |Reason Shows a "held" item and should be held by right hand in rtl | |
|configure-shortcuts | "held items should be right handed" | IGNORAR |
|configure-toolbars | "held items should be right handed"  | IGNORAR |
|configure    | "held items should be right handed" | IGNORAR|
|document-sign| "held items should be right handed" | NO SE |
|games-config-custom  |   | IGNORAR  |
|games-config-options |   | IGNORAR  |
|im-voice-irc         |   | NO SE    |


Reason
Reflect direction of text flow

|     |     |    |
|-----|-----|----|
|document-export | export arrow to left                   |  REDESIGN OR NOT NEED   |
|document-import | import arrow to right                  |  REDESIGN OR NOT NEED   |
|edit-redo | arrow direction should reflect text direction|  REDESIGN OR OK         |
|edit-undo | arrow direction should reflect text direction|  REDESIGN OR OK         |
|format-indent-less | shows ltr format|    OK                         |
|format-indent-more | shows rtl format|    OK                         |
|games-difficult    |                 |    ?                          |
|games-endturn      | ?               |    ? or NOT NEED              |
|go-previous-*      |                 |    OK                         |
|mail-forward       | ?               |                               |
|mail-reply-*       | ?               |                               |
|office-chart-* |charts axis show lrt |                               |


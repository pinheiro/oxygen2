import os
from pathlib import Path
import logging

logging.basicConfig(level=logging.DEBUG, filename='./movelinks.log', encoding = 'utf-8', format = '%(asctime)s %(levelname)s: %(message)s')
 
'''
(c) Antonio Elrío 2023
Distributed under MIT LICENSE. 
NO WARRANTY ON USE.
'''
BASE = './Icons'

logging.info("### Starting {} ".format('movelinks'))

for root, dirs, files in os.walk(BASE):
    if root.find('symbolic') == -1 and root.find('scalable') == -1: # No estamos en simlinks ni en scalable que tienen todos esos org.kde links
        for f in files:
            builded_path = os.path.join(root,f)
           
            #logging.debug("{} is link? {}".format(builded_path, os.path.islink(builded_path)))
            if os.path.islink(builded_path): 
                logging.debug("Builded_path {}".format(builded_path))
                #builded_path represents the actual link.
                try:
                    #dest: the new LINK: ./Icons/symbolic/{CUE} of actual link.
                    #src: the actual FILE as a relative path to the link being created.
                    
                    dest = os.path.join(BASE,"symbolic", os.path.relpath(Path(builded_path), start=BASE))
                    # desde cada link conseguir el path relativo. Asi hay links que no se rompen
                    src = os.path.relpath(Path(builded_path).resolve(), start=os.path.split(dest)[0] ) 
                    
                    logging.info("TRY TO CREATE LINK {} pointing to file {}".format(dest, src))
                    os.symlink(src, dest)
                    logging.info("LINK DONE")
                    
                except Exception as ex:
                    logging.error("Couldn't create simlink: {} => {}".format(src,dest))
                    logging.error(ex)

logging.info("### Ended {} ".format('movelinks'))

'''
WORK BASE

First LINK(builded_path)
.Icons/              size/cat/link.png

I want to make link at 
.Icons/symbolic/size/cat/link.png


SRC is BUILDED_PATH(link) resolved and RELATIVE to DEST.

'''

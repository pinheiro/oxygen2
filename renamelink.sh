#!/bin/bash
###########################################
#Name           : renamelink.sh
#Description    : Creates the link in the same icon folder.
#Author         : Antonio Elrío
#mail           : oscoder87@gmail.com
#
# Distributed under no warranty.
###########################################

# Check if exactly 2 arguments were passed
if [ $# -ne 2 ]; then
  echo "Usage: $0 <file_reference> <new_filename_without_extension>"
  exit 1
fi

# Initialization
file_ref="$1"
new_filename="$2"

# Directory of file which will be linked
file_dir="$(dirname "$file_ref")"
cd $file_dir

# Filename w/o extension and extension
file_base="$(basename "${file_ref%.*}")"

file_ext="${file_ref##*.}"

# Construct the new filename with extension
new_filename_with_ext="${new_filename}.${file_ext}"

# Construct the paths for the new file and the link
new_file_path="${file_dir}/${new_filename_with_ext}"
link_path="${file_dir}/${file_base}.${file_ext}"

# Copy the file and remove the original file (mv)
# Create a symbolic link with the old filename that points to the new file
mv -fv "$file_base.$file_ext" "$new_filename_with_ext" && ln -sv "$new_filename_with_ext" "$file_base.$file_ext"


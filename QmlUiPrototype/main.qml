import QtQuick 2.12
import QtQuick.Window 2.11
import QtGraphicalEffects 1.0

Window {
    id: root
    visible: true
    width: 500*scalingArea.scale
    height: 400*scalingArea.scale
    color:"transparent"
    flags: Qt.FramelessWindowHint

    function getBrighness(rgb) {
        return (rgb.r * 299 + rgb.g * 587 + rgb.b * 114) / 1000;
    }
    DragHandler {
                  acceptedDevices: PointerDevice.GenericPointer
                  grabPermissions:  PointerHandler.CanTakeOverFromItems || PointerHandler.CanTakeOverFromHandlersOfDifferentType || PointerHandler.ApprovesTakeOverByAnything
                  onActiveChanged: if (active) root.startSystemMove()
              }
    QtObject {
        id: style
        property color basecolor: "#18171a"//"#fdfbfc" //
        Behavior on basecolor {ColorAnimation {duration: 900}}
        readonly property bool dark: getBrighness(basecolor)<0.5

        readonly property color oulineNColour: Qt.hsla((style.basecolor.hslHue*1.05),(style.basecolor.hslSaturation*(dark?1.3:0.2)),(style.basecolor.hslLightness+(dark?0.25:-0.4)),style.basecolor.a)
        readonly property color oulineOutColour: Qt.hsla((style.basecolor.hslHue*1.05),(style.basecolor.hslSaturation*(dark?1.3:0.2)),(style.basecolor.hslLightness+(dark?0.19:-0.11)),style.basecolor.a)
        readonly property color basecolorNoise : Qt.hsla((style.basecolor.hslHue*1.05),(style.basecolor.hslSaturation*(dark?0.6:0.1)),(style.basecolor.hslLightness+(dark?0.08:-0.05)),style.basecolor.a)

        readonly property color butonInColour: Qt.hsla((style.basecolor.hslHue),(style.basecolor.hslSaturation*(dark?0.4:0.4)),(style.basecolor.hslLightness+(dark?-0.25:+0.4)),style.basecolor.a)
        property color baseTextcolor: dark? "#a5a5a5": "#252525"
        property color baseTextcolorHovered: dark? "#c5c5c5": "#050505"
    }

Item {
    id: scalingArea
    z:2
    width: parent.width/scale
    height: parent.height/scale
    anchors.verticalCenterOffset: 0
    anchors.horizontalCenterOffset: 0
    anchors.centerIn: parent
    scale: 1

    BorderImage { id: deco

        verticalTileMode: BorderImage.Repeat
        horizontalTileMode: BorderImage.Repeat
        source: "./Images/dbg.png"
        width: scalingArea.width - x*2-1
        height: scalingArea.height -y*2
        x: root.visibility === 4 ? -11 : 0
        y: root.visibility === 4 ? -10 : 0
        border.left: 30; border.top: 30
        border.right: 80; border.bottom: 30

        Rectangle {
            id: outlineBaseRect
            anchors.fill: baseColorRect
            anchors.margins: -1
            radius: baseColorRect.radius+1
            color: style.oulineOutColour

        }
        Rectangle {
            id: baseColorRect
         x:12
         y:11
         height: parent.height-22
         width: parent.width-23
         color: style.basecolor
         radius: 5
        }
        BorderImage {
            id: noise
            visible: false
            verticalTileMode: BorderImage.Repeat
            horizontalTileMode: BorderImage.Repeat
            source: "./Images/noise.png"
          anchors.fill: baseColorRect
            border.left: 5; border.top: 5
            border.right: 5; border.bottom: 5
        }
        OpacityMask{
            anchors.fill: noise
            maskSource: noise
            source: Rectangle {
               width: 10
               height: 10
              color: style.basecolorNoise
            }


        }
       Row{
           anchors.top: parent.top
           anchors.topMargin: 18
           anchors.right: parent.right
           anchors.rightMargin: 18
           spacing: 6

        Buton {
            x: 521
            width: 14
            height: 14
            onClicked:  root.showMinimized()
            radius: height

        }

        Buton {
            x: 539
            width: 14
            height: 14
            radius: height
            onClicked: root.visibility === 4 ? root.showNormal() : root.showMaximized()

        }

        Buton {
            width: 14
            height: 14
            anchors.top: parent.top
            onClicked: root.close()
            pressColor: "red"
            outlineColour: style.dark? "#b0c90000": "#b0a00000"
            radius: height
        }
       }


    }

    Text {
        id: progressText
        text: visualItem.progress2+"%"
        font.family: "Open Sans Light"
        font.pointSize: 8+20*parent.scale
        color: "white"
       anchors.centerIn: visualItem
        anchors.horizontalCenterOffset: 7
        opacity: 0.5 * visualItem.opacity
        scale:0.7
    }
        VisualItem {
            id: visualItem


            z:50
            property  int progress2: 0
            property real opaci: running? 1 : 0
            running: progress2>0
            opacity:  opaci + (1-scalei)
            property real scalei: progress2 === 100 ? 2 : 1
            anchors.verticalCenterOffset: 12
            anchors.horizontalCenterOffset: -130
            scale: scalei*0.7

            Behavior on scalei {
                NumberAnimation {
                    duration: 1200
                    easing.type: Easing.InQuad
                }
            }

            Behavior on opaci {

                NumberAnimation {
                    duration: 4200
                    easing.type: Easing.InOutQuad
                }
            }

            Behavior on progress2 {

                NumberAnimation {
                    duration: 16200
                    easing.type: Easing.InOutQuad
                }
            }

            MouseArea {
                anchors.fill: parent
                onClicked: visualItem.progress2=100
            }
        }

        GaussianBlur {
            anchors.fill: visualItem
            source: visualItem
            radius: 16
            samples: 9
            transparentBorder: true
            opacity: visualItem.progress2/100*visualItem.opacity
            scale: 1.05*visualItem.scale

        }

        Progress {
            id: progress
            x: 336
            y: 164

            width: 96
            small: true
            radius: height/2
            anchors.right: parent.right
            anchors.rightMargin: 168

            SequentialAnimation {
                id: progressLoop

                NumberAnimation {
                    target: progress
                    property: "progress"
                    duration: Math.random()*6000+26000
                    easing.type: Easing.InOutQuad
                    to: 100
                }
                NumberAnimation {
                    target: progress
                    property: "progress"
                    duration: 3200
                    easing.type: Easing.InOutQuad
                    to: 0
                }
                loops: -1

            }
        }
        Progress{
            x: 336
            y: 193
            width: 242
            anchors.right: parent.right
            anchors.rightMargin: 22
            radius: height/2
            progress: progress.progress


        }
        Progress{
            y: 229
            width: 242
            anchors.right: parent.right
            anchors.rightMargin: 22
            progress: progress.progress
        }

        Progress{
            y: 164
            width: 130
            small: true
            anchors.right: parent.right
            anchors.rightMargin: 22

            progress: progress.progress

        }

        Buton {
            id: buton
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 22
            text: "Save"
            onClicked:style.basecolor = "#18171a"
        }
        Buton {
            id: buton32
            anchors.bottom: buton.bottom
            anchors.right: buton.left
            anchors.rightMargin: 12
            text: "Cancel"
            onClicked: {
            progressLoop.stop()
            onClicked:style.basecolor = "#fefbfc"
            }
         }

//        Row { anchors.bottom: buton.top
//            anchors.right: buton.right
//            spacing: 8
//            anchors.bottomMargin: 8
//            Buton {
//                width: 50
//                height: 50
//                onClicked: style.basecolor="#fddffd"
//                Rectangle {
//                    anchors.fill: parent
//                    color: "#fddffd"
//                    radius: 4
//                    anchors.margins: 6
//                }
//            }
//            Buton {
//                width: 50
//                height: 50
//                anchors.bottomMargin: 8
//                onClicked: style.basecolor="#fdfbfc"
//                Rectangle {
//                    anchors.fill: parent
//                    color: "#fdfbfc"
//                    radius: 4
//                    anchors.margins: 6
//                }
//            }
//            Buton {
//                width: 50
//                height: 50
//                anchors.bottomMargin: 8
//                onClicked: style.basecolor="#fbf0d7"
//                Rectangle {
//                    anchors.fill: parent
//                    color: "#fbf0d7"
//                    radius: 4
//                    anchors.margins: 6
//                }
//            }
//        Buton {
//            width: 50
//            height: 50
//            onClicked: style.basecolor="#182129"
//            Rectangle {
//                anchors.fill: parent
//                color: "#182129"
//                radius: 4
//                anchors.margins: 6
//            }
//        }
//        Buton {
//            width: 50
//            height: 50
//            anchors.bottomMargin: 8
//            onClicked: style.basecolor="#292c3b"
//            Rectangle {
//                anchors.fill: parent
//                color: "#292c3b"
//                radius: 4
//                anchors.margins: 6
//            }
//        }
//        Buton {
//            width: 50
//            height: 50

//            anchors.bottomMargin: 8
//            onClicked: style.basecolor="#18171a"
//            Rectangle {
//                anchors.fill: parent
//                color: "#18171a"
//                radius: 4
//                anchors.margins: 6
//            }
//        }


//        }

        Buton {
            id: buton2
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 22
            anchors.bottomMargin: 58
            text: "Start Progress"
            radius: height/2
            onClicked: {
            progressLoop.start()
            }
        }
        Buton {
            id: buton22
            anchors.bottom: buton2.bottom
            anchors.right: buton.left
            anchors.rightMargin: 12
            text: "Cancel1"
            radius: height/2


        }
        Buton {
            id: buton3
            x: 514
            y: 286
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 22
            anchors.bottomMargin: 94
            text: "Spin Back"
            small: true
            width: 64
            height: 20
            onClicked: {
            visualItem.progress2 = 0
            }
        }
        Buton {
            id: buton23
            x: 427
            y: 286
            text: "Start Spiner"
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 94
            anchors.right: parent.right
            anchors.rightMargin: 101
            radius: height/2
            small: true
            width: 72
            height: 20
            onClicked: {
            visualItem.progress2 = 100
            }
        }
    }



//Text {
//    id: color2
//    x: 42
//    y:50
//    text: getBrighness(style.basecolor)+"  "+ (style.basecolor.hslLightness*3.6)+"  "+ (style.basecolor.hslLightness+0.25)
//    width: 40
//    height: width
//    color: style.oulineOutColour
//  }

}


/*##^##
Designer {
    D{i:0;formeditorZoom:1.5}D{i:16;anchors_height:20;anchors_width:115}
}
##^##*/

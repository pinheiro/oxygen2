﻿import QtQuick 2.11
import QtQuick.Window 2.11
import QtGraphicalEffects 1.0
Item{
    id: buton
    width: 115
    height: small? 18 : 25
    property bool hovered:  bMArea.containsMouse &&  !dissablehover.running
    property bool small: false
    signal clicked()

    property color hovereColorT: hovered ? style.baseTextcolorHovered: style.baseTextcolor
    Behavior on  hovereColorT {ColorAnimation {duration: 360 ; id: ca4 } }

    property bool dark: style.dark
    property color pressColor: style.oulineOutColour
    property color outlineColour: style.oulineNColour
    property color hovereColor1: hovered? "#53949f": outlineColour
    property color hovereColor2: hovered? "#755579": outlineColour
    Behavior on  hovereColor1 { ColorAnimation {duration: 360 ; id: ca1 } }
    Behavior on  hovereColor2 { ColorAnimation {duration: 360 ; id: ca2 } }


    onHoveredChanged: if (hovered)
                      {
                          ca1.duration = 360
                          ca2.duration = 360
                          ca3.duration = 360
                          ca4.duration = 360
                      }
                      else
                      {   ca1.duration = 80
                          ca2.duration = 80
                          ca3.duration = 80
                          ca4.duration = 80
                      }

    property bool pressed : bMArea.pressed
    property alias text: text.text
    property int radius : small? 5:6

    MouseArea {
        id: bMArea
        anchors.fill: parent

        hoverEnabled: true
        anchors.margins: - 3
        onReleased: dissablehover.start()
        onEntered: dissablehover.stop()
        onExited: dissablehover.stop()
        onClicked: buton.clicked()
        drag.target: bMArea //here to prevent drag events to pass

    }
    Timer{
        id: dissablehover
        interval: 1500
    }
    Rectangle{
        id: outLine
        visible: false
        anchors.fill: parent
        color: "transparent"
        border.color: "white"
        border.width: 1
        radius: buton.radius
        anchors.rightMargin: 0
        anchors.bottomMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: 0

        Rectangle{
            anchors.fill: parent
            anchors.margins: 2
            color: bMArea.containsMouse? "#40ffffff" : "transparent"
            radius: buton.radius - anchors.margins + 1
        }
    }

    RadialGradient {
        id: maskColor
        source: outLine
        anchors.fill: outLine
        property int grane: height / 4
        property int smallMouseX: bMArea.mouseX / grane
        property int smallMouseY: bMArea.mouseY / grane

        horizontalRadius: width
        verticalRadius: width
        horizontalOffset: smallMouseX * grane - width / 2
        verticalOffset: smallMouseY * grane - height / 2

        gradient: Gradient {
            GradientStop { position: 0.0; color: pressed? pressColor : hovereColor1}
            GradientStop { position: 0.7; color: pressed? pressColor : hovereColor2}
        }
    }

    Rectangle {
        anchors.fill: parent
        anchors.margins: 1
        radius: outLine.radius-outLine.border.width
        property color hovereColor1: hovered ? (Qt.rgba(style.butonInColour.r, style.butonInColour.g, style.butonInColour.b, 0.2 ) ): dark? "#30000000": "#30FFFFFF"
        Behavior on  hovereColor1 {ColorAnimation {duration: 360 ; id: ca3 } }
        color: pressed ? style.butonInColour: hovereColor1
    }
    Text {
        id: text
        font.pointSize: small? 7.5:9
        anchors.centerIn: parent
        text: ""
        styleColor: dark? "#b3000000" : "#b3ffffff"
        style: Text.Raised
        renderType: Text.NativeRendering
        color: pressed?"#656565": hovereColorT
    }
}

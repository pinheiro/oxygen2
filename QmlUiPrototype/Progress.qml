import QtQuick 2.11
import QtQuick.Window 2.11
import QtGraphicalEffects 1.0
Item{
    id: progress
    width: 250
    height: small? 18:25
    property bool hovered:  bMArea.containsMouse &&  !dissablehover.running
    property bool pressed : bMArea.pressed
    property real progress: 50
    property int radius: 6
    property bool small: false
    property color textColour: style.baseTextcolor
    property color styletextColour: style.dark? "#50000000": "#60ffffff"
    MouseArea {
        id: bMArea
        anchors.fill: parent
        hoverEnabled: true
        onReleased: dissablehover.start()
        onEntered: dissablehover.stop()
        onExited: dissablehover.stop()
    }
    Timer{
        id: dissablehover
        interval: 1000
    }
    Rectangle{
        id: outLine
        anchors.fill: parent
        color: Qt.rgba(style.butonInColour.r, style.butonInColour.g, style.butonInColour.b, 0.5 )
        border.color: style.oulineNColour
        border.width: 1
        radius: progress.radius
    }
Item{
    id: masked
    anchors.fill: parent
    visible: false
    Rectangle{
        id: bar
        height: parent.height-6
        x:3
        y:3
        radius: progress.radius-2.5
        width: Math.max( (radius > height/2.1 ? height: radius*2)  ,
                        ((progress.width - 6) * progress.progress/100 ) ) //making sure round remains round and non round are as ammal as possible
        color: "#60ffffff"
        border.color: "#fff"
        Item {
            height: outLine.height+2
            anchors.verticalCenter: parent.verticalCenter
            anchors.right: parent.right
            anchors.rightMargin: -progress.progress/100*progress.progress/100*progress.progress/100*3  //making sure its close to the edge of the progress but run away in the end to the full with
            anchors.left: parent.left
            anchors.leftMargin: -3
            clip: true
            Rectangle {
                width: progress.width
                height: parent.height-2
                y: 1
                anchors.rightMargin: -progress.radius/2-3
                color: "#00000000"
                border.color: "#70ffffff"
                border.width: 1
                radius: progress.radius
            }
        }
    }
}

    LinearGradient {
        id: maskColor
        visible: false
        anchors.fill: outLine
        start: Qt.point(0, height)
        end: Qt.point(150-progress.progress, 0)
        gradient: Gradient {
            GradientStop { position: 0.0; color: "#11c2e1"}
            GradientStop { position: 1.0; color: "#893a94"}
        }
    }

    OpacityMask {
        anchors.fill: masked
        source: maskColor
        maskSource: masked
        opacity: ((progress.width-6)*progress.progress/100)/bar.width

    }

    Text {
        id: text
        font.pointSize: 9
        text: Math.round(progress.progress)+"%"
        styleColor: styletextColour
        style: Text.Outline
        renderType: Text.NativeRendering
        color: textColour
        anchors.right: parent.right
        anchors.verticalCenter: parent.verticalCenter
        anchors.rightMargin: 6
    }
}

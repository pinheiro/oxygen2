
import QtQuick 2.11
import QtQuick.Window 2.11
import QtGraphicalEffects 1.0
Window {
    visible: true
    width: 400
    height: 400
    title: qsTr("Hello World")

    color:"white"
    flags: Qt.FramelessWindowHint
    opacity: 1



    VisualItem {
        id: visualItem
    }

    GaussianBlur {
        anchors.fill: visualItem
        source: visualItem
        radius: 16
        samples: 9
        transparentBorder: true
        opacity: 1
        scale: 1.05
    }

}

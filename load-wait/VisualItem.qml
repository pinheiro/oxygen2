import QtQuick 2.11
import QtQuick.Window 2.11
import QtGraphicalEffects 1.0
Item{
    id: visualItem
    property bool runing: false
    width: 229
    height: 229
    
    anchors.centerIn: parent
    


    Image {
        id: cyan
        source: "./Images/blue.png"
    }
    Image {
        id: magenta
        source: "./Images/magenta.png"
    }
    Image {
        id: yellow
        source: "./Images/lime.png"
    }
    
    NumberAnimation {
        running: true
        target: cyan
        property: "rotation"
        duration: 1400
        loops:-1
        from:0
        to: 360
    }
    NumberAnimation {
        running: true
        target: magenta
        property: "rotation"
        duration: 2000
        loops: -1
        from:0
        to: 360
    }
    NumberAnimation {
        running: true
        target: yellow
        property: "rotation"
        duration: 2600
        loops: -1
        from:0
        to: 360
    }
    SequentialAnimation {
        running: true
        onStopped: {
            cyan.z=magenta.z+1
            running=true
        }
        
        NumberAnimation {
            target: cyan
            property: "opacity"
            duration: 4000
            easing.type: Easing.OutExpo
            from: 0
            to: 1
        }
        NumberAnimation {
            target: cyan
            property: "opacity"
            duration: 1000
            easing.type: Easing.InOutQuad
            from: 1
            to: 0
        }
    }
    SequentialAnimation {
        running: true
        onStopped: {
            magenta.z=yellow.z+1
            running = true
        }
        NumberAnimation {
            target: magenta
            property: "opacity"
            duration: 4500
            easing.type: Easing.OutExpo
            from: 0
            to: 1
        }
        NumberAnimation {
            target: magenta
            property: "opacity"
            duration: 1000
            easing.type: Easing.InOutQuad
            from: 1
            to: 0
        }
    }
    SequentialAnimation {
        running: true
        onStopped: {
            yellow.z=cyan.z+1
            running=true
        }
        NumberAnimation {
            target: yellow
            property: "opacity"
            duration: 5000
            easing.type: Easing.OutExpo
            from: 0
            to: 1
        }
        NumberAnimation {
            target: yellow
            property: "opacity"
            duration: 1000
            easing.type: Easing.InOutQuad
            from: 1
            to: 0
        }
    }
}
